/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lp1_f7g2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author 1181539
 */
public class LP1_F7G2 {
    private static final Scanner scanner = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String[][] dadosCidades = getDadosCidades();
        
        System.out.println("1 - Mostrar temperatura máxima de uma cidade");
        System.out.println("2 - Mostrar temperatura miníma de uma cidade");
        System.out.println("3 - Mostrar temperatura média de uma cidade");
        System.out.println("4 - Mostrar temperatura miníma de um dia");
        System.out.println("6 - Mostrar temperatura média de um dia");
        System.out.println("7 - Mostrar temperatura mais baixa");
        System.out.println("8 - Mostrar temperatura mais alta de um dia");
        System.out.println("5 - Mostrar temperatura máxima de um dia");
        int opcao = getInt("");
        switch(opcao){
            case 1: mostrarTemperaturaMax(dadosCidades);
                    break;
            case 2: mostrarTemperaturaMin(dadosCidades);
                    break;
            case 3: mostrarTemperaturaMedia(dadosCidades);
                    break;
            case 4: temperaturaMinimaDia(dadosCidades);
                    break;
            case 5: mostrarTemperaturaMax1Dia(dadosCidades);
                    break;
            case 6: temperaturaMediaDia(dadosCidades);
                    break;
            case 7: mostrarTemperaturaMaisBaixa(dadosCidades);
                    break;
            case 8: mostrarTemperaturaMaisAlta(dadosCidades);
                    break;
        }
        
    }
    
    public static void mostrarTemperaturaMax(String[][] dadosCidades) {
        double tempMax = 0.0;
     
        for (int j = 0; j < 11; j++) {
            if (j > 0) {
                if (Double.parseDouble(dadosCidades[0][j]) > tempMax) {
                    tempMax = Double.parseDouble(dadosCidades[0][j]);
                }
            }
        }
        
        System.out.println("Temperatura máxima do Porto é " + tempMax);
    }
    
    public static void mostrarTemperaturaMin(String[][] dadosCidades) {
        double tempMin = 60.0;
     
        for (int j = 0; j < 11; j++) {
            if (j > 0) {
                if (Double.parseDouble(dadosCidades[0][j]) < tempMin) {
                    tempMin = Double.parseDouble(dadosCidades[0][j]);
                }
            }
        }
        
        System.out.println("Temperatura minima do porto é " + tempMin);
    }
    
    public static void mostrarTemperaturaMedia(String[][] dadosCidades) {
        double totalTemp = 0.0;
     
        for (int j = 0; j < 11; j++) {
            if (j > 0) {
                totalTemp = totalTemp + Double.parseDouble(dadosCidades[0][j]);
            }
        }
        
        System.out.println("Temperatura média do Porto é " + (totalTemp / 10));
    }
    
    public static void mostrarTemperaturaMax1Dia(String[][] dadosCidades) {
        double tempMax = 0.0;
     
        for(int i = 0 ; i < dadosCidades.length ; i++ ){
            if (dadosCidades[i][1] != null){
                if (Double.parseDouble(dadosCidades[i][1]) > tempMax) {
                    tempMax = Double.parseDouble(dadosCidades[i][1]);
                }
            }
        }
        
        System.out.println("Temperatura máxima do 1º dia é " + tempMax);
    }
    
    public static void mostrarTemperaturaMaisBaixa(String[][] dadosCidades) {
        double tempMin = 999.0;
     
        for(int i = 0 ; i < dadosCidades.length ; i++ ){
            if (dadosCidades[i][1] != null){
                for (int j = 0; j < 11; j++) {
                    if (j > 0) {
                        if (Double.parseDouble(dadosCidades[i][j]) < tempMin) {
                            tempMin = Double.parseDouble(dadosCidades[i][j]);
                        }
                    }
                }
            }
        }
        
        System.out.println("Temperatura mínima de todas é " + tempMin);
    }
    public static boolean ficheiroExiste(String nomeFicheiro){
        File f = new File("D:/lp1_ficha07/LP1_F7G2/" + nomeFicheiro);
        return f.exists();
    }
    
    public static String[][] getDadosCidades(){
        String[][] retValue = new String[99][13];
        
        if(ficheiroExiste("temperaturas.txt")) {
            try { 
                FileReader fich = new FileReader("D:/lp1_ficha07/LP1_F7G2/temperaturas.txt");
                BufferedReader lerFich = new BufferedReader(fich);
                
                String linha = lerFich.readLine();
                int count = 0;
                
                while (linha != null) {
                    String[] dados = linha.split("-");
                    retValue[count][0] = dados[0];
                    retValue[count][1] = dados[1];
                    retValue[count][2] = dados[2];
                    retValue[count][3] = dados[3];
                    retValue[count][4] = dados[4];
                    retValue[count][5] = dados[5];
                    retValue[count][6] = dados[6];
                    retValue[count][7] = dados[7];
                    retValue[count][8] = dados[8];
                    retValue[count][9] = dados[9];
                    retValue[count][10] = dados[10];
                    
                    linha = lerFich.readLine();
                    count++;
                }

                fich.close();
            } catch (IOException e) {
                System.out.println("Erro!");
            }
        } 
        
        return retValue;
    }
    
    public static int getInt(String msg) {
        int retValue = 0;
        boolean isInt = false;

        System.out.print(msg);
        
        while (!isInt) {
            try {
                retValue = scanner.nextInt();
                isInt = true;
            } catch (InputMismatchException ex) {
                scanner.next();
                System.out.println("Número inválido. Introduza um número inteiro.");
                System.out.print(msg);
            }
        }
  
        return retValue;
    }
    
    public static void temperaturaMinimaDia(String[][] dadosCidades){
        double tempMin = 1000.00;
        String cidadeTempMin = "";
        for(int i = 0 ; i < dadosCidades.length ; i++){
            if(dadosCidades[i][1] != null){
                for(int j = 0 ; j < 1 ; j++){
                    if(Double.parseDouble(dadosCidades[i][1]) < tempMin){
                        tempMin = Double.parseDouble(dadosCidades[i][1]);
                        cidadeTempMin = dadosCidades[i][0];
                    }
                }
            }
        }
        System.out.println("A cidade com a temperatura miníma do 1º dia é : " + cidadeTempMin + " com : " + tempMin + "ºC graus");
    }
    
    public static void temperaturaMediaDia(String[][] dadosCidades){
        double somaTemperaturas = 0.0;
        
        for(int i = 0 ; i < dadosCidades.length ; i++){
            if(dadosCidades[i][1] != null){
                somaTemperaturas = somaTemperaturas + Double.parseDouble(dadosCidades[i][1]);
            }
        }
        System.out.println("A média de temperaturas do 1º dia é : " + (somaTemperaturas / 3));
    }
    
    public static void mostrarTemperaturaMaisAlta(String[][] dadosCidades) {
        double tempMax = 0.0;
     
        for(int i = 0 ; i < dadosCidades.length ; i++ ){
            if (dadosCidades[i][1] != null){
                for (int j = 0; j < 11; j++) {
                    if (j > 0) {
                        if (Double.parseDouble(dadosCidades[i][j]) > tempMax) {
                            tempMax = Double.parseDouble(dadosCidades[i][j]);
                        }
                    }
                }
            }
        }
        
        System.out.println("Temperatura maxima de todas é " + tempMax);
    }
}
